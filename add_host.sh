#!/bin/bash

# hosts file path
HOSTS_PATH="/etc/hosts"
# MAC - vhosts apache path - MAMP default is: /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf
VHOSTS_APACHE_PATH="/Applications/MAMP/conf/apache/extra/httpd-vhosts.conf"
# Linux - vhosts patch
LINUX_VHOSTS_APACHE_PATH="/etc/apache2/sites-available"
# project directory
PROJECTS_DIRECTORY="/Users/mateuszbielinski/www"
#default port 
PORT="80"
#localhost ip address
IP_ADDRESS="127.0.0.1"
#restart MAMP(Mac) apache command
RESTART_COMMAND="/Applications/MAMP/Library/bin/apachectl restart"
#restart Linux apache command
LINUX_RESTART_COMMAND="service apache2 reload"

echo "Please enter directory name: "
read NEW_PROJECT_DIRECTORY

echo "Please enter new hostname: "
read NEW_HOSTNAME

echo "Create directory in www folder? (y or n)"
read CREATE_DIRECTORY

echo "Are you sure? (y or n): "
read SURE

OS=`uname`
if [ "$SURE" = "y" ]; then
	#Mac
	if [ "$OS" == "Darwin" ]; then
		#add to MAC apache file
		echo "" >> $VHOSTS_APACHE_PATH
		echo "<VirtualHost *:$PORT>" >> $VHOSTS_APACHE_PATH
		echo "	DocumentRoot $PROJECTS_DIRECTORY/$NEW_PROJECT_DIRECTORY" >> $VHOSTS_APACHE_PATH
		echo "	ServerName $NEW_HOSTNAME" >> $VHOSTS_APACHE_PATH
		echo "</VirtualHost>" >> $VHOSTS_APACHE_PATH
	fi
	#Linux
	if [ "$OS" = "Linux" ]; then
		echo "<VirtualHost *:$PORT>" >> "$LINUX_VHOSTS_APACHE_PATH/$NEW_HOSTNAME.conf"
		echo "	DocumentRoot $PROJECTS_DIRECTORY/$NEW_PROJECT_DIRECTORY" >> "$LINUX_VHOSTS_APACHE_PATH/$NEW_HOSTNAME.conf"
		echo "	ServerName $NEW_HOSTNAME" >> "$LINUX_VHOSTS_APACHE_PATH/$NEW_HOSTNAME.conf"
		echo "</VirtualHost>" >> "$LINUX_VHOSTS_APACHE_PATH/$NEW_HOSTNAME.conf"
		a2ensite $NEW_HOSTNAME
	fi
	
	#create directory if choose
	if [ "$CREATE_DIRECTORY" = "y" ]; then
		mkdir -p "$PROJECTS_DIRECTORY/$NEW_PROJECT_DIRECTORY"
		echo "<h1>hello world</h1>" >> "$PROJECTS_DIRECTORY/$NEW_PROJECT_DIRECTORY/index.html"
		echo "<p>content generated automatically</p>" >> "$PROJECTS_DIRECTORY/$NEW_PROJECT_DIRECTORY/index.html"
		echo "<p>have fun</p>" >> "$PROJECTS_DIRECTORY/$NEW_PROJECT_DIRECTORY/index.html"
		echo "<a href='http://mateuszbielinski.pl' target='_blank'>www.mateuszbielinski.pl</a>" >> "$PROJECTS_DIRECTORY/$NEW_PROJECT_DIRECTORY/index.html"
	fi

	#add to hosts file
	sudo sh -c "echo '$IP_ADDRESS       $NEW_HOSTNAME' >> $HOSTS_PATH"

	if [ "$OS" = "Darwin" ]; then
		#restart Mac apache to load vhosts
		$RESTART_COMMAND
	fi

	if [ "$OS" = "Linux" ]; then
		#restart Linux apache to load vhosts
		$LINUX_RESTART_COMMAND
	fi

    echo "Successfuly added $NEW_HOSTNAME hostname"
else
    echo "Abort! Goodbye!"
fi
